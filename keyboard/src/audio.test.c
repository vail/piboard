#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>

static char *device = "default"; /* playback device */
snd_output_t *output = NULL;
unsigned char buffer[16 * 1024]; /* some random data */

int main(void)
{
	int err;
	unsigned int i;
	snd_pcm_t *handle;
	snd_pcm_sframes_t frames;
	snd_pcm_uframes_t bufferSize, periodSize;

	for (i = 0; i < sizeof(buffer); i++)
		buffer[i] = random() & 0xff;

	if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}
	if ((err = snd_pcm_set_params(handle,
		SND_PCM_FORMAT_S16_LE,
		SND_PCM_ACCESS_RW_INTERLEAVED,
		1, //channels
		44100, //sample rate
		1, //allow resampling
		500000) //required latency in us
		) < 0) {
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	if ((err = snd_pcm_prepare(handle)) < 0) {
		printf("Pcm prepare error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	if ((err = snd_pcm_get_params(handle, &bufferSize, &periodSize)) < 0) {
		printf("Pcm get params error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}
	printf("Buffer size:%d, Period size:%d\n", (int)bufferSize, (int)periodSize);

	for (i = 0; i < 16; i++) {
		frames = snd_pcm_writei(handle, buffer, periodSize);
		if (frames < 0)
			frames = snd_pcm_recover(handle, frames, 0);
		if (frames < 0) {
			printf("snd_pcm_writei failed: %s\n", snd_strerror(err));
			break;
		}
		if (frames > 0 && frames < (long)periodSize)
			printf("Short write (expected %li, wrote %li)\n", (long)sizeof(buffer), frames);
	}
	snd_pcm_close(handle);
	return 0;
}