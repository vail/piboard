
#include "audio.h"


int main(void)
{
	/*
	wiringPiSetup();

	pinMode(A_KEY, INPUT);
	pinMode(B_KEY, INPUT);
	pinMode(C_KEY, INPUT);
	pinMode(D_KEY, INPUT);
	pinMode(E_KEY, INPUT);
	pinMode(F_KEY, INPUT);
	pinMode(G_KEY, INPUT);
	BOOL pool;
	BOOL prev;
	int length = 7;
	
	while (1)
	{
		delayMicroseconds(50);
		pool = digitalRead(A_KEY);
		pool ^= digitalRead(B_KEY) << 1;
		pool ^= digitalRead(C_KEY) << 2;
		pool ^= digitalRead(D_KEY) << 3;
		pool ^= digitalRead(E_KEY) << 4;
		pool ^= digitalRead(F_KEY) << 5;
		pool ^= digitalRead(G_KEY) << 6;
		
		if (pool != prev)
		{
			printf("Val: %d ", pool);
			for (int i = 0; i < 7; i++)
			{
				char value = (pool << i);
				printf("%d", !!value);
				if (value > 0)
				{
					sound(pool);
				}
			}
			prev = pool;
		}
	}*/

	timing();

	return 0;
}

//Value in the buffer seems to determine the volume of the sound being played.

void sound(char value)
{
	int err;
	unsigned int i;
	snd_pcm_t *handle;
	snd_pcm_sframes_t frames;
	snd_pcm_uframes_t bufferSize, periodSize;
	int channels = 1;
	int sampleRate = 44100;
	int resampling = 1;
	int latency = 400000;

	printf("Value: %d ", value);
	for (i = 0; i < sizeof(buffer); i++)
	{
		buffer[i] = 0xFF;
	}

	err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0);
	if (err < 0) 
	{
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	err = snd_pcm_set_params(handle, SND_PCM_FORMAT_S16_LE, SND_PCM_ACCESS_RW_INTERLEAVED, channels, sampleRate, resampling, latency);
	if (err < 0) 
	{
		printf("Playback open error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	err = snd_pcm_prepare(handle);
	if (err < 0) 
	{
		printf("Pcm prepare error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}

	err = snd_pcm_get_params(handle, &bufferSize, &periodSize);
	if (err < 0) 
	{
		printf("Pcm get params error: %s\n", snd_strerror(err));
		exit(EXIT_FAILURE);
	}
	printf("Buffer size:%d, Period size:%d\n", (int)bufferSize, (int)periodSize);

	for (i = 0; i < 16; i++)
	{
		frames = snd_pcm_writei(handle, buffer, periodSize);
		if (frames < 0)
		{
			frames = snd_pcm_recover(handle, frames, 0);
		}
		if (frames < 0) 
		{
			printf("snd_pcm_writei failed: %s\n", snd_strerror(err));
			break;
		}
		if (frames > 0 && frames < (long)periodSize)
		{
			printf("Short write (expected %li, wrote %li)\n", (long)sizeof(buffer), frames);
		}
	}
	snd_pcm_close(handle);
}

int generate_sine(int steps, short** buffer, int frequency)
{
	*buffer = calloc(steps, sizeof(short));
	if (*buffer == NULL)
	{
		return 1;
	}
	for (int i = 0; i < steps; ++i)
	{
		**buffer = 0xFF * sin(frequency * i);
	}
	return 0;
}