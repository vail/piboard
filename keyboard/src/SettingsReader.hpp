#pragma once

#define _USE_MATH_DEFINES
#include <array>
#include <math.h>

struct Settings
{
	float diameter;
	float density;
	float youngModulus;
	float initLength;
	float stopPosition;
	double solveInCoeff(double);
};

constexpr int MAX_NOTE_COUNT = 88;
constexpr int MAX_BUFFER_SIZE = 150;

// Precomputation to make the calculations faster
extern std::array<double, MAX_NOTE_COUNT * MAX_BUFFER_SIZE> wavePos;
extern std::array<double, MAX_NOTE_COUNT * MAX_BUFFER_SIZE> sincArray;
extern std::array<double, MAX_NOTE_COUNT * MAX_BUFFER_SIZE> timeWave;
extern std::array<double, MAX_NOTE_COUNT> trueFrequency;
extern std::array<double, MAX_NOTE_COUNT> frequency;
extern std::array<double, MAX_NOTE_COUNT> coeffs;

// Can reduce this to be dynamic if memory is an issue
extern std::array<Settings, MAX_NOTE_COUNT> configuration;
extern int configSize;
void DetermineFrequency();
