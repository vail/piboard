#pragma once

#include <wiringPi.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <sched.h>
#include <sys/time.h>
#include <alsa/asoundlib.h>
#include <math.h>

static char* device = "default"; /* playback device */
snd_output_t* output = NULL;
unsigned char buffer[16 * 512]; /* some random data */

static unsigned int rate = 44100;                       /* stream rate */
static unsigned int channels = 1;                       /* count of channels */
static unsigned int buffer_time = 500000;               /* ring buffer length in us */
static unsigned int period_time = 100000;               /* period time in us */
static const float PI2 = 9.8696044010893586188;

typedef char BOOL;

// LED Pin - wiringPi pin 0 is BCM_GPIO 17.
// we have to use BCM numbering when initializing with wiringPiSetupSys
// when choosing a different pin number please use the BCM numbering, also
// update the Property Pages - Build Events - Remote Post-Build Event command 
// which uses gpio export for setup for wiringPiSetupSys
#define	LED	17
#define A_KEY 23
#define B_KEY 24
#define C_KEY 25
#define D_KEY 26
#define E_KEY 27
#define F_KEY 28
#define G_KEY 29

void sound(char);
int generate_sine(int steps, short** buffer, int frequency);

//The temporal decay to make things sound a bit more natural
static float timeDecay[20000];

//The collection of sin waves. There are 88 possible keys that can be played on 
//the keyboard.
static float sinWaves[88][20000];

//Looks like there is no simple expression for knowing when to add the new harmonic.
//It might be a good idea to look at knowing which notes are played. Apply a 
//damping harmonic to each note so it can keep playing until the values go to zero
///when the note is released.

//Counters might be needed for each note played to know long it has been played for.
//Need to figure out how much time is needed between each signal sweep to know how
//big the buffers need to be.
void timing(void)
{
	unsigned long long first_time;
	unsigned long long second_time;
	unsigned long long time_difference;
	struct timespec gettime_now;

	for (int i = 0; i < 20000; ++i)
	{
		*(timeDecay + i) = expf(-0.0056 * i);
	}
}

/**
 * This function determines the inharmonics that are played for every note and 
 * fills in the respecitve wave arrays. The inharmonic (or non-equal or structured tuning)
 * is defined as w = kv*sqrt(v^2 + Ak^2)
 */
void determineInharmonics(int position);


/**
 *	This function determines the harmonics that are played for every note and
 *  fills in the respecive wave arrays. The harmonic (or equal tuning) is defined
 *  as w = kv
 */
void determineHarmonics(int position);