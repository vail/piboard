
#include "SettingsReader.hpp"

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <vector>

constexpr int MAX_LINE_SIZE = 99;

// The multiplier that modifies each freqency
float multiplier = 440.0;
int ReadLine(FILE *fptr, char *line)
{
	int ch, i = 0;
	while((ch = getc_unlocked(fptr)) != '\n')
	{
		if(i < MAX_LINE_SIZE)
		{
			*(line + i++) = ch;
		}
	}
	*(line + i) = '\0';
	return i;
}

std::vector<std::string> ReadLines(std::string filename)
{
	std::vector<std::string> lines;
	std::ifstream stream(filename);
	std::string line;
	while(std::getline(stream, line))
	{
		lines.push_back(line);
	}
}

double Settings::solveInCoeff(double frequency)
{
	float youngModulus = this->youngModulus;
	float diameter = this->diameter;
	float density = this->density;
	float length = this->initLength;
	double divisor = 128 * density * length * frequency;
	return M_PI * M_PI * youngModulus * diameter / divisor;
}

void ReadSettings(FILE *fptr, Settings &settings, int *start)
{
	char *line = (char *)malloc(MAX_LINE_SIZE * sizeof(char));
	float *vals = (float *)malloc(6 * sizeof(float));
	int index = 0;
	int endVal = 0;
	ReadLine(fptr, line);
	for(int i = 0; i < 7; ++i)
	{
		char *ptr = strsep(&line, ",");
		vals[index++] = strtof(ptr, NULL);
	}
	settings.diameter = vals[0];
	settings.density = vals[1];
	settings.youngModulus = vals[2];
	settings.initLength = vals[3];
	*start = endVal;

	free(line);
	free(vals);
}

void ReadFile()
{
	char file[] = "settings.ini";
	FILE *fptr = fopen(file, "r");
	char *line = (char *)malloc(MAX_LINE_SIZE * sizeof(char));

	if(fptr != NULL)
	{
		ReadLine(fptr, line);
		char *ptr;
		int total = strtol(line, ptr, 10);
		configSize = total;
		int index = 0;
		int start = 0;

		for(int i = 0; i < total; ++i)
		{
			ReadSettings(fptr, configuration[i], &start);
		}
		fclose(fptr);
	}
	free(line);
}

void AdjustFrequency()
{
	int settingsUsed[MAX_NOTE_COUNT];
	float lengths[MAX_NOTE_COUNT];

	int index = 0;
	int range = configuration[0].startNote - configuration[0].endNote;
	for(int i = 0; i < MAX_NOTE_COUNT; i++)
	{
		if(i - configuration[index].startNote >= range)
		{
			settingsUsed[i] = index;
			lengths[i] =
				configuration[index].initLength * trueFrequency[i] / trueFrequency[configuration[index].startNote];
		}
		else
		{
			settingsUsed[i] = ++index;
			lengths[i] = configuration[index].initLength;
		}
	}

	for(int i = 0; i < i < MAX_NOTE_COUNT; ++i)
	{
		Settings settings = configuration[settingsUsed[i]];
		double l = (double)lengths[i];
		double freq = (double)trueFrequency[i];
		double young = (double)settings.youngModulus;
		double diam = (double)settings.diameter;
		double density = (double)settings.density;
		double coeff = M_PI * M_PI * young * diam / (128 * density * l * l * l * l * freq * freq);
		frequency[i] = freq * (1 + coeff);
		coeffs[i] = coeff;
	}
}

void DetermineFrequency()
{
	multiplier /= pow(2, 49 / 12);
	for(int i = 0; i < MAX_NOTE_COUNT; i++)
	{
		int val = ((i + 1) << 2);
		float value = pow(val, 1 / 12);
		trueFrequency[i] = value * multiplier;
	}
	ReadFile();

	// Check if the read file returns a non-zero configuration file
	if(configSize > 0)
	{
		AdjustFrequency();
	}
	else
	{
		for(int i = 0; i < MAX_NOTE_COUNT; ++i)
		{
			frequency[i] = trueFrequency[i];
		}
	}
}
