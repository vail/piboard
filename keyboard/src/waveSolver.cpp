#include <math.h>

#include "waveSolver.h"

static char prevSolved = 0;
double posWave[MAX_BUFFER_SIZE];

inline double solveSinc(int n, double frequency)
{
	double val = divisor / (n * n * frequency * (1 - frequency));
	return val * sin(n * PI * frequency);
}

inline double solveSinPosition(int n, double ratio)
{
	return sin(n * PI * ratio);
}

inline double solveSinTime(int n, double timeStep, double frequency, SettingsP configuration)
{
	double stiffness = configuration->coeff(configuration, frequency);
	return sin(2 * PI * n * timeStep * frequency * sqrt(1 + stiffness));
}

double* solveTimeWave(const int count, const double timeStep, const double frequency, const SettingsP configuration)
{
	double* timeWave = malloc(sizeof(double) * count);
	for (int i = 0; i < count; ++i)
	{
		*(timeWave + i) = solveSinTime(i, timeStep, frequency, configuration);
	}
	return timeWave;
}

double* solveSincPos(const int count,const double frequency)
{
	double* sincPos = malloc(sizeof(double) * count);
	for (int i = 0; i < count; ++i)
	{
		*(sincPos + i) = solveSinc(i, frequency);
	}
	return sincPos;
}

double* solveWavePos(const int count, const double ratio)
{
	double* wavePos = malloc(sizeof(double) * count);
	for (int i = 0; i < count; ++i)
	{
		*(wavePos + i) = solveSinPosition(i, ratio);
	}
	return wavePos;
}

double* solveWave(double frequency, double timeStep, SettingsP configuration, int count)
{
	double* timeWave = solveTimeWave(count, timeStep, frequency, configuration);
	double* sincVals = solveSincPos(count, frequency);
	double ratio = configuration->initLength / configuration->stopPosition;
	double* wavePos = solveWavePos(count, ratio);

	double* wave = malloc(sizeof(double) * count);
	for (int i = 0; i < count; ++i)
	{
		*(wave + i) = timeWave[i] * sincVals[i] * wavePos[i];
	}
	free(timeWave);
	free(sincVals);
	free(wavePos);

	return wave;
}