#pragma once

#include "SettingsReader.hpp"

const double divisor = 2 / (PI * PI);

extern double posWave[MAX_BUFFER_SIZE];

double extern solveSinc(int n, double frequency);

double extern solveSinTime(int n, double frequency, SettingsP timeAmount);
