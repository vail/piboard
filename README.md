# Raspberry PI digitial piano

Turning the raspberry pi (rpi) into a digitial piano. The ultimate plan is to create an entire 88 key digital piano with the petals using the rpi's GPIO ports that can be fully customized.

Interestingly I feel into a rabbit's hole to figure out how to implement such a thing. Rather than implementing a digitial sound board or MIDI controller and be done with it, I decided to go to the fundamentals and determine how a piano works and model the sound it crates and have it output that sound.

Currently this is implemented in C as the hardware layer is being worked on. Parts may be implemented in C++ after the hardware layer is finished.
# Build dependacies

Rpi v3B or greater
gcc
cmake

Project will use cmake to contain all the possible options