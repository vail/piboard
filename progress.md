# RPi Piano Project

There are two main things to do:

- Write the piano player
- Write the settings adjustment

## Settings
The settings adjustment will the same components as the piano player but does not require the raspberry pi.

 - Most of the GUI will be developed in Qt
 - All it would allow you to do would be is to configure the settings for the piano

The major components needed for it would be:

 -    Reading and Writing the settings
 -   Converting the settings to a piano string
  -  Project what the setting would sound like
  -  Auto-fill missing settings for a particular note

### Breakdown
The settings needed are:

 - E - young's modulus
 - R - radius of the string
 - rho - linear density
 - L - length of the string
 - f - base frequency of the note

The base frequency is calculated as \\(f = c/2L \\) where \\(c\\) is the speed of sound
Inharmonic frequency is \\[f_n = nf \sqrt{1 + Bn^2}\\]

