# Parent project does not export its library target, so this CML implicitly
# depends on being added from it, i.e. the testing is done only from the build
# tree and is not feasible from an install location

project(PiBoardTests LANGUAGES CXX)

# ---- Dependencies ----

find_package(doctest REQUIRED)
find_package(yaml-cpp REQUIRED)
# add_library(PiBoard)

# ---- Tests ----

file(GLOB_RECURSE SOURCE_FILES CONFIGURE_DEPENDS
     ${CMAKE_CURRENT_LIST_DIR}/source/*.cpp)

add_executable(${PROJECT_NAME} ${SOURCE_FILES})

target_include_directories(
  ${PROJECT_NAME}
  PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
         $<INSTALL_INTERFACE:include> # relative to CMAKE_INSTALL_PREFIX
  PRIVATE ${PROJECT_BINARY_DIR}) # for generated header files, like config
#
target_link_libraries(
  ${PROJECT_NAME} PUBLIC PiBoard::core PiBoard::settings PiBoard::engine
                         PiBoard::setup doctest::doctest yaml-cpp::yaml-cpp)

# ---- End-of-file commands ----
