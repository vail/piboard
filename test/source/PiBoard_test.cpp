#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "settings-setup.h"

#include <doctest/doctest.h>
#include <string>

using namespace engine;
using namespace engine::setup;

TEST_CASE("Test Arguments and Saving")
{
	std::vector<std::string> testStrings = {"", "-s"};
	Setup setup = Setup(testStrings);
	auto configs = setup.GetConfiguration();
	setup.saveConfig("export.config");
}
