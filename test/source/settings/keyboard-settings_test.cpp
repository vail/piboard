
#include "keyboard-settings.h"
#include "system-settings.h"

#include <doctest/doctest.h>

using namespace engine;
using namespace engine::settings;

namespace engine::settings
{
	class StringSettingsTester
	{
	  public:
		static void TestCalculate(StringSettings& object) { object.Calculate(); }
		static double TestCalculateTension(StringSettings& object, int value) { return object.CalculateTension(value); }
	};
} // namespace engine::settings

// NOLINTBEGIN
// Test cases for the StringSettings class
TEST_CASE("StringSettings class tests")
{
	SUBCASE("Constructor tests")
	{
		StringSettings settings;
		CHECK(settings.Modulus() == doctest::Approx(0.0));
		CHECK(settings.Radius() == doctest::Approx(0.0));
		CHECK(settings.Density() == doctest::Approx(0.0));
		CHECK(settings.Length() == doctest::Approx(0.0));
		CHECK(settings.BaseFrequency() == doctest::Approx(0.0));
		CHECK(settings.Inharmonic() == doctest::Approx(0.0));
		CHECK(settings.Tension() == doctest::Approx(0.0));
		CHECK(!settings.IsValid());
	}

	SUBCASE("Parameterized Constructor tests")
	{
		StringSettings settings(1.0, 2.0, 3.0, 4.0);
		CHECK(settings.Modulus() == doctest::Approx(1.0));
		CHECK(settings.Radius() == doctest::Approx(2.0));
		CHECK(settings.Density() == doctest::Approx(3.0));
		CHECK(settings.Length() == doctest::Approx(4.0));
		CHECK(settings.BaseFrequency() == doctest::Approx(0.0));
		CHECK(settings.Inharmonic() == doctest::Approx(0.0));
		CHECK(settings.Tension() == doctest::Approx(0.0));
		CHECK(settings.IsValid());
	}

	SUBCASE("Copy Constructor test")
	{
		StringSettings original(1.0, 2.0, 3.0, 4.0);
		StringSettings copy(original);

		CHECK(copy.Modulus() == doctest::Approx(1.0));
		CHECK(copy.Radius() == doctest::Approx(2.0));
		CHECK(copy.Density() == doctest::Approx(3.0));
		CHECK(copy.Length() == doctest::Approx(4.0));
		CHECK(copy.BaseFrequency() == doctest::Approx(0.0));
		CHECK(copy.Inharmonic() == doctest::Approx(0.0));
		CHECK(copy.Tension() == doctest::Approx(0.0));
		CHECK(copy.IsValid());
	}

	SUBCASE("Move Constructor test")
	{
		StringSettings original(1.0, 2.0, 3.0, 4.0);
		StringSettings moved(std::move(original));

		CHECK(moved.Modulus() == doctest::Approx(1.0));
		CHECK(moved.Radius() == doctest::Approx(2.0));
		CHECK(moved.Density() == doctest::Approx(3.0));
		CHECK(moved.Length() == doctest::Approx(4.0));
		CHECK(moved.BaseFrequency() == doctest::Approx(0.0));
		CHECK(moved.Inharmonic() == doctest::Approx(0.0));
		CHECK(moved.Tension() == doctest::Approx(0.0));
		CHECK(moved.IsValid());
	}

	SUBCASE("Copy Assignment Operator test")
	{
		StringSettings original(1.0, 2.0, 3.0, 4.0);
		StringSettings copy;
		copy = original;

		CHECK(copy.Modulus() == doctest::Approx(1.0));
		CHECK(copy.Radius() == doctest::Approx(2.0));
		CHECK(copy.Density() == doctest::Approx(3.0));
		CHECK(copy.Length() == doctest::Approx(4.0));
		CHECK(copy.BaseFrequency() == doctest::Approx(0.0));
		CHECK(copy.Inharmonic() == doctest::Approx(0.0));
		CHECK(copy.Tension() == doctest::Approx(0.0));
		CHECK(copy.IsValid());
	}

	SUBCASE("Move Assignment Operator test")
	{
		StringSettings original(1.0, 2.0, 3.0, 4.0);
		StringSettings moved;
		moved = std::move(original);

		CHECK(moved.Modulus() == doctest::Approx(1.0));
		CHECK(moved.Radius() == doctest::Approx(2.0));
		CHECK(moved.Density() == doctest::Approx(3.0));
		CHECK(moved.Length() == doctest::Approx(4.0));
		CHECK(moved.BaseFrequency() == doctest::Approx(0.0));
		CHECK(moved.Inharmonic() == doctest::Approx(0.0));
		CHECK(moved.Tension() == doctest::Approx(0.0));
		CHECK(moved.IsValid());
	}

	SUBCASE("Equality Operator test")
	{
		StringSettings settings1(1.0, 2.0, 3.0, 4.0);
		StringSettings settings2(1.0, 2.0, 3.0, 4.0);
		StringSettings settings3(1.0, 2.0, 3.0, 5.0);

		CHECK(settings1 == settings2);
		CHECK(!(settings1 == settings3));
	}

	SUBCASE("Frequency test")
	{
		StringSettings settings(1.0, 2.0, 3.0, 4.0);
		double frequency = settings.Frequency(5);

		CHECK(frequency == doctest::Approx(0.0)); // Assuming the default value for Frequency is 0.0
	}

	SUBCASE("GetInharmonics test")
	{
		StringSettings settings(1.0, 2.0, 3.0, 4.0);
		SystemConfiguration system = SystemConfiguration();
		std::vector<double> inharmonics = settings.GetInharmonics(5, system);

		CHECK(inharmonics.empty());
	}

	// Add more test cases as needed
}

TEST_CASE("StringSettings class testing private functions")
{
	SUBCASE("Calculate Test")
	{
		StringSettings settings(1.0, 2.0, 3.0, 4.0);
		StringSettingsTester::TestCalculate(settings);
		CHECK(settings.Modulus() == doctest::Approx(1.0));
		CHECK(settings.Radius() == doctest::Approx(2.0));
		CHECK(settings.Density() == doctest::Approx(3.0));
		CHECK(settings.Length() == doctest::Approx(4.0));
		CHECK(settings.BaseFrequency() == doctest::Approx(0.0));
		CHECK(settings.Inharmonic() == doctest::Approx(0.0));
		CHECK(settings.Tension() == doctest::Approx(0.0));
		CHECK(settings.IsValid());
	}

	SUBCASE("Calculate Tension Test")
	{
		StringSettings settings(1.0, 2.0, 3.0, 4.0);
		double returnValue = StringSettingsTester::TestCalculateTension(settings, 2);

		CHECK(returnValue == doctest::Approx(0.0));
	}
}

// NOLINTEND
