
#include "properties.h"

#include <doctest/doctest.h>
#include <string>

using namespace engine;

// NOLINTBEGIN

// Test case for the Property class
TEST_CASE("Property class tests")
{
	// Test construction and value retrieval
	SUBCASE("Construction and value retrieval")
	{
		Property<int> intProperty(42);
		Property<double> doubleProperty(3.14);

		CHECK(intProperty() == 42);
		CHECK(doubleProperty() == doctest::Approx(3.14));
	}

	// Test assignment and value retrieval
	SUBCASE("Assignment and value retrieval")
	{
		Property<std::string> stringProperty("Hello");
		stringProperty = "World";

		CHECK(stringProperty() == "World");
	}

	// Add more test cases as needed
}

// NOLINTEND
