#include "vector.h"

#include <doctest/doctest.h>

using namespace engine;

// NOLINTBEGIN

// Test cases for the Vec3d class
TEST_CASE("Vec3d class tests")
{
	SUBCASE("Constructor tests")
	{
		Vec3d vec(1.0, 2.0, 3.0);

		CHECK(vec.XPos() == doctest::Approx(1.0));
		CHECK(vec.YPos() == doctest::Approx(2.0));
		CHECK(vec.ZPos() == doctest::Approx(3.0));
	}

	SUBCASE("Operator== test")
	{
		Vec3d vec1(1.0, 2.0, 3.0);
		Vec3d vec2(1.0, 2.0, 3.0);
		Vec3d vec3(1.0, 2.0, 4.0);

		CHECK(vec1 == vec2);
		CHECK(!(vec1 == vec3));
	}

	SUBCASE("Operator= test")
	{
		Vec3d vec1(1.0, 2.0, 3.0);
		Vec3d vec2(4.0, 3.0, 2.0);

		vec1 = vec2;

		CHECK(vec1 == vec2);
	}

	SUBCASE("Operator* test")
	{
		Vec3d vec(1.0, 2.0, 3.0);
		double scalar = 2.0;

		Vec3d result = vec * scalar;

		CHECK(result.XPos() == doctest::Approx(2.0));
		CHECK(result.YPos() == doctest::Approx(4.0));
		CHECK(result.ZPos() == doctest::Approx(6.0));
	}

	SUBCASE("Operator+ test")
	{
		Vec3d vec1(1.0, 2.0, 3.0);
		Vec3d vec2(4.0, 5.0, 6.0);

		Vec3d result = vec1 + vec2;

		CHECK(result.XPos() == doctest::Approx(5.0));
		CHECK(result.YPos() == doctest::Approx(7.0));
		CHECK(result.ZPos() == doctest::Approx(9.0));
	}

	SUBCASE("Dot product test")
	{
		Vec3d vec1(1.0, 2.0, 3.0);
		Vec3d vec2(4.0, 5.0, 6.0);

		double result = Dot(vec1, vec2);

		CHECK(result == doctest::Approx(32.0));
	}

	SUBCASE("Cross product test")
	{
		Vec3d vec1(1.0, 2.0, 3.0);
		Vec3d vec2(4.0, 5.0, 6.0);

		Vec3d result = Cross(vec1, vec2);

		CHECK(result.XPos() == doctest::Approx(-3.0));
		CHECK(result.YPos() == doctest::Approx(6.0));
		CHECK(result.ZPos() == doctest::Approx(-3.0));
	}

	// Add more test cases as needed
}

// NOLINTEND
