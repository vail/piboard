
#include "position.h"
#include "vector.h"

#include <doctest/doctest.h>

using namespace engine;

// NOLINTBEGIN
TEST_CASE("Position class tests")
{
	SUBCASE("Constructor tests")
	{
		Position pos1(1.0, 2.0, 3.0, 4.0);
		Position pos2(Vec3d(1.0, 2.0, 3.0), 4.0);

		CHECK(pos1.SpacePosition() == Vec3d(1.0, 2.0, 3.0));
		CHECK(pos1.TimePosition() == doctest::Approx(4.0));
		CHECK(pos2.SpacePosition() == Vec3d(1.0, 2.0, 3.0));
		CHECK(pos2.TimePosition() == doctest::Approx(4.0));
	}

	SUBCASE("Operator== test")
	{
		Position pos1(1.0, 2.0, 3.0, 4.0);
		Position pos2(1.0, 2.0, 3.0, 4.0);
		Position pos3(1.0, 2.0, 3.0, 5.0);

		CHECK(pos1 == pos2);
		CHECK(!(pos1 == pos3));
	}

	SUBCASE("Operator= test")
	{
		Position pos1(1.0, 2.0, 3.0, 4.0);
		Position pos2(4.0, 3.0, 2.0, 1.0);

		pos1 = pos2;

		CHECK(pos1 == pos2);
	}

	SUBCASE("Operator+ test")
	{
		Position pos1(1.0, 2.0, 3.0, 4.0);
		Vec3d vec(1.0, 1.0, 1.0);
		double scalar = 2.0;

		Position result1 = pos1 + vec;
		Position result2 = pos1 + scalar;

		CHECK(result1.SpacePosition() == Vec3d(2.0, 3.0, 4.0));
		CHECK(result1.TimePosition() == doctest::Approx(4.0));

		CHECK(result2.TimePosition() == doctest::Approx(6.0));
	}

	SUBCASE("Operator+= test")
	{
		Position pos1(1.0, 2.0, 3.0, 4.0);
		Vec3d vec(1.0, 1.0, 1.0);
		double scalar = 2.0;

		pos1 += vec;
		pos1 += scalar;

		CHECK(pos1.SpacePosition() == Vec3d(2.0, 3.0, 4.0));
		CHECK(pos1.TimePosition() == doctest::Approx(6.0));
	}

	// Add more test cases as needed
}

// NOLINTEND
