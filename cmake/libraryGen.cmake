set(SRCS ${CMAKE_CURRENT_LIST_DIR}/../src/main.cpp)

find_package(fmt REQUIRED)

add_library(PiBoard_lib OBJECT src/lib.cpp)

target_include_directories(
  PiBoard_lib ${warning_guard}
  PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
         $<INSTALL_INTERFACE:include> # relative to CMAKE_INSTALL_PREFIX
  PRIVATE "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>")

target_compile_features(PiBoard_lib PUBLIC cxx_std_20)

target_link_libraries(PiBoard_lib PRIVATE fmt::fmt)
