install(
    TARGETS PiBoard_exe
    RUNTIME COMPONENT PiBoard_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
