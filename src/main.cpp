#include "settings-setup.h"

#include <iostream>
#include <string>

int main(int argc, char** argv)
{
	engine::setup::Setup setup = engine::setup::Setup(argc, argv);
	std::cout << '\n';
	return 0;
}
