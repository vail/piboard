
#pragma once

#include <cmath>
#include <limits>
#include <type_traits>
#include <utility>

namespace engine
{
	template<typename T>
	class Property
	{
	  protected:
		T value; // NOLINT

	  public:
		Property(const T& _value) : value(std::move(_value)) {}
		Property(const Property&) = default;
		Property(Property&&) = default;

		Property& operator=(const Property&) = delete;
		Property& operator=(Property&&) = delete;
		virtual ~Property() = default;

		virtual T& operator=(const T& f) { return value = f; }
		virtual const T& operator()() const { return value; }
		virtual explicit operator const T&() const { return value; }
		virtual T* operator->() { return &value; }
	};
} // namespace engine
