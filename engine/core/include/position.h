
#pragma once

#include "properties.h"
#include "vector.h"

namespace engine
{
	class Position
	{
	  public:
		Position(double xPos, double yPos, double zPos, double time);
		Position(const Vec3d& pos, double time);
		Position(const Position&) = default;
		Position(Position&&) = delete;
		~Position() = default;

		bool operator==(const Position& rhs) const;
		Position& operator=(const Position& rhs);
		Position& operator=(Position&&) = delete;

		Position operator+(const Vec3d& rhs) const;
		Position operator+(double rhs) const;
		Position& operator+=(const Vec3d& rhs);
		Position& operator+=(double rhs);

		Property<Vec3d> SpacePosition;
		Property<double> TimePosition;
	};
} // namespace engine
