
#pragma once

#include "properties.h"

#include <cmath>
#include <limits>

namespace engine
{
	class Vec3d
	{
	  public:
		Vec3d(double xPos, double yPos, double zPos);
		Vec3d(const Vec3d&) = default;
		Vec3d(Vec3d&&) = default;

		Vec3d& operator=(const Vec3d&);
		Vec3d& operator=(Vec3d&&) = delete;
		~Vec3d() = default;

		inline static bool Compare_Double(double x, double y)
		{
			return std::fabs(x - y) < std::numeric_limits<double>::epsilon();
		}

		bool operator==(const Vec3d& rhs) const;
		Vec3d operator*(double rhs);

		friend Vec3d operator+(Vec3d lhs, const Vec3d& rhs);
		friend double Dot(const Vec3d& lhs, const Vec3d& rhs);
		friend Vec3d Cross(const Vec3d& lhs, const Vec3d& rhs);

		Property<double> XPos;
		Property<double> YPos;
		Property<double> ZPos;
	};
} // namespace engine
