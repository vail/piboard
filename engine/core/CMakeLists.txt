cmake_minimum_required(VERSION 3.23)
set(component core)
set(component_lib ${CMAKE_PROJECT_NAME}-${component})
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

set(SHORT_INCLUDE_DIR ${CMAKE_CURRENT_LIST_DIR}/include/)
set(SHORT_SRC_DIR ${CMAKE_CURRENT_LIST_DIR}/src/)

file(GLOB PUB_HDRS CONFIGURE_DEPENDS ${SHORT_INCLUDE_DIR}/*.h)

file(GLOB SRCS CONFIGURE_DEPENDS ${SHORT_SRC_DIR}/*cpp)

add_library(${component} STATIC ${PUB_HDRS} ${SRCS})

set_target_properties(${component} PROPERTIES OUTPUT_NAME "${component_lib}")

add_library(${CMAKE_PROJECT_NAME}::${component} ALIAS ${component})

target_compile_features(
  ${component}
  PUBLIC cxx_std_20
  PRIVATE)

target_include_directories(
  ${component}
  PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
         $<INSTALL_INTERFACE:include> # relative to CMAKE_INSTALL_PREFIX
  PRIVATE ${PROJECT_BINARY_DIR}) # for generated header files, like config
# header

target_link_libraries(
  ${component}
  PUBLIC
  PRIVATE)
