
#include "position.h"

#include <cmath>
#include <limits>

namespace engine
{
	Position::Position(double xPos, double yPos, double zPos, double time) :
		SpacePosition({xPos, yPos, zPos}), TimePosition(time)
	{
	}

	Position::Position(const Vec3d& pos, double time) : SpacePosition(pos), TimePosition(time) {}

	bool Position::operator==(const Position& rhs) const
	{
		bool timeCheck = (std::fabs(TimePosition() - rhs.TimePosition())) < std::numeric_limits<double>::epsilon();
		return SpacePosition() == rhs.SpacePosition() && timeCheck;
	}

	Position& Position::operator=(const Position& rhs)
	{
		SpacePosition = rhs.SpacePosition();
		TimePosition = rhs.TimePosition();
		return *this;
	}

	Position& Position::operator+=(const Vec3d& rhs)
	{
		SpacePosition = SpacePosition() + rhs;
		return *this;
	}

	Position& Position::operator+=(double rhs)
	{
		TimePosition = TimePosition() + rhs;
		return *this;
	}

	Position Position::operator+(const Vec3d& rhs) const
	{
		return {SpacePosition() + rhs, TimePosition()};
	}

	Position Position::operator+(double rhs) const
	{
		return {SpacePosition(), TimePosition() + rhs};
	}
} // namespace engine
