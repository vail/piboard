
#include "vector.h"

namespace engine
{
	Vec3d::Vec3d(double xPos, double yPos, double zPos) : XPos(xPos), YPos(yPos), ZPos(zPos) {}

	Vec3d& Vec3d::operator=(const Vec3d& other)
	{
		XPos = other.XPos();
		YPos = other.YPos();
		ZPos = other.ZPos();

		return *this;
	}

	bool Vec3d::operator==(const Vec3d& rhs) const
	{
		bool xCompare = Compare_Double(XPos(), rhs.XPos());
		bool yCompare = Compare_Double(YPos(), rhs.YPos());
		bool zCompare = Compare_Double(ZPos(), rhs.ZPos());
		return (xCompare && yCompare && zCompare);
	}

	Vec3d Vec3d::operator*(double rhs)
	{
		XPos = XPos() * rhs;
		YPos = YPos() * rhs;
		ZPos = ZPos() * rhs;
		return *this;
	}

	Vec3d operator+(Vec3d lhs, const Vec3d& rhs)
	{
		lhs.XPos = lhs.XPos() + rhs.XPos();
		lhs.YPos = lhs.YPos() + rhs.YPos();
		lhs.ZPos = lhs.ZPos() + rhs.ZPos();
		return lhs;
	}

	double Dot(const Vec3d& lhs, const Vec3d& rhs)
	{
		return lhs.XPos() * rhs.XPos() + lhs.YPos() * rhs.YPos() + lhs.ZPos() * rhs.ZPos();
	}

	Vec3d Cross(const Vec3d& lhs, const Vec3d& rhs)
	{
		double xPos = lhs.YPos() * rhs.ZPos() - lhs.ZPos() * rhs.YPos();
		double yPos = lhs.ZPos() * rhs.XPos() - lhs.XPos() * rhs.ZPos();
		double zPos = lhs.XPos() * rhs.YPos() - lhs.YPos() * rhs.XPos();
		return {xPos, yPos, zPos};
	}

} // namespace engine
