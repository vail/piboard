
#pragma once

#include "keyboard-settings.h"
#include "system-settings.h"

#include <string>
#include <vector>

namespace engine::setup
{
	class Control
	{
	  private:
		Control() = default;
		std::vector<settings::StringSettings> stringSettings;
		settings::SystemConfiguration configuration;

	  public:
		static Control& GetHandle();

		[[nodiscard]] settings::SystemConfiguration GetConfiguration() const;
		bool ImportConfiguration(const std::string& configurationName);
		bool ExportConfiguration(const std::string& configurationName);

		[[nodiscard]] std::vector<settings::StringSettings> GetSettings() const;
		bool ImportSettings(const std::string& settingsName);
		bool ExportSettings(const std::string& settingsName);
	};

} // namespace engine::setup
