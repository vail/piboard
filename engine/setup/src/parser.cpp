
#include "parser.h"

#include "keyboard-settings.h"
#include "system-settings.h"

#include <fstream>
#include <string>
#include <yaml-cpp/node/emit.h>
#include <yaml-cpp/node/impl.h>
#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>

namespace engine::setup
{
	Control& Control::GetHandle()
	{
		static Control instance;
		return instance;
	}

	settings::SystemConfiguration Control::GetConfiguration() const
	{
		return configuration;
	}

	bool Control::ImportConfiguration(const std::string& configurationName)
	{
		if(configurationName.empty())
		{
			return false;
		}

		auto tot = YAML::LoadAllFromFile(configurationName);
		for(const auto& itr : tot)
		{
			configuration = itr.as<settings::SystemConfiguration>();
			break;
		}
		return true;
	}

	bool Control::ExportConfiguration(const std::string& configurationName)
	{
		if(configurationName.empty())
		{
			return false;
		}

		std::ofstream fout(configurationName);
		YAML::Node local;
		local = configuration;

		fout << local;
		return true;
	}

	std::vector<settings::StringSettings> Control::GetSettings() const
	{
		return stringSettings;
	}

	bool Control::ImportSettings(const std::string& settingsName)
	{
		if(settingsName.empty())
		{
			return false;
		}

		auto tot = YAML::LoadAllFromFile(settingsName);
		for(auto& itr : tot)
		{
			auto settings = itr.as<settings::StringSettings>();
			stringSettings.push_back(settings);
		}

		int index = 0;
		for(auto& settings : stringSettings)
		{
			settings.Frequency(index);
		}
		return true;
	}

	bool Control::ExportSettings(const std::string& settingsName)
	{
		if(settingsName.empty())
		{
			return false;
		}

		std::ofstream fout(settingsName);

		for(const auto& settings : stringSettings)
		{
			YAML::Node local;
			local = settings;
			fout << local;
		}

		return true;
	}
} // namespace engine::setup
