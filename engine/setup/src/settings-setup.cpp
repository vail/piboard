
#include "settings-setup.h"

#include "keyboard-settings.h"
#include "parser.h"
#include "system-settings.h"

#include <argparse/argparse.hpp>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <optional>
#include <string>
#include <vector>

namespace engine::setup
{
	const std::vector<std::string_view> Setup::definitionOptionsArgs = {"-l", "-s", "-ls", "--load", "--save"};
	const std::vector<Options> Setup::definitionOptions = {Options::LOAD,
														   Options::SAVE,
														   Options::LOAD_AND_SAVE,
														   Options::LOAD,
														   Options::SAVE};

	Setup::Setup(int argc, char** argv) : argumentParser("Settings Controller")
	{
		// Flag for allowing verbose processing
		argumentParser.add_argument("--verbose").help("Allow for verbose processing").flag();

		argumentParser.add_description(
			"This application will create (and afterwards import) for a piano configuration");
		argumentParser.add_argument("-l", "--load").help("Specify where to import the settings file");
		argumentParser.add_argument("-s", "--save").help("Specify where to export the settings file");
		init(argc, argv);
	}

	Setup::Setup(std::vector<std::string> args) : argumentParser()
	{
		argumentParser.add_argument("--verbose").help("Allow for verbose processing").flag();

		argumentParser.add_description(
			"This application will create (and afterwards import) for a piano configuration");
		argumentParser.add_argument("-l", "--load").help("Specify where to import the settings file");
		argumentParser.add_argument("-s", "--save").help("Specify where to export the settings file");
		init(args);
	}

	void Setup::init(std::vector<std::string> args)
	{
		try
		{
			argumentParser.parse_args(args);
		}
		catch(const std::exception& err)
		{
			std::cerr << err.what() << std::endl;
			std::cerr << argumentParser;
			std::exit(1);
		}
	}

	void Setup::init(int argc, char** argv)
	{
		try
		{
			argumentParser.parse_args(argc, argv);
		}
		catch(const std::exception& err)
		{
			std::cerr << err.what() << std::endl;
			std::cerr << argumentParser;
			std::exit(1);
		};
	}

	Options Setup::GetOptions()
	{
		Options selected = Options::NO_OPTION;
		if(auto isLoad = argumentParser.present("-l"))
		{
			selected = Options::LOAD;
		}
		if(auto isSave = argumentParser.present("-s"))
		{
			// Should probably use it as a flag
			if(selected == Options::LOAD)
			{
				selected = Options::LOAD_AND_SAVE;
			}
			else
			{
				selected = Options::SAVE;
			}
		}
		return selected;
	}

	std::vector<settings::StringSettings> Setup::GetSettings() const
	{
		Control fileSettings = Control::GetHandle();
		return fileSettings.GetSettings();
	}

	void Setup::loadSettings(const std::string& filename)
	{
		Control fileSettings = Control::GetHandle();
		fileSettings.ImportSettings(filename);
	}

	void Setup::saveSettings(const std::string& filename)
	{
		Control fileSettings = Control::GetHandle();
		fileSettings.ExportSettings(filename);
	}

	settings::SystemConfiguration Setup::GetConfiguration() const
	{
		Control fileSettings = Control::GetHandle();
		return fileSettings.GetConfiguration();
	}

	void Setup::loadConfig(const std::string& filename)
	{
		Control fileSettings = Control::GetHandle();
		fileSettings.ImportConfiguration(filename);
	}
	void Setup::saveConfig(const std::string& filename)
	{
		Control fileSettings = Control::GetHandle();
		fileSettings.ExportConfiguration(filename);
	}
} // namespace engine::setup
