
#pragma once

#include "keyboard-settings.h"
#include "system-settings.h"

#include <argparse/argparse.hpp>
#include <string>
#include <string_view>
#include <vector>

namespace engine::setup
{
	enum class Options
	{
		NO_OPTION = 0,
		LOAD,
		SAVE,
		LOAD_AND_SAVE,
		HELP
	};

	class Setup
	{
	  private:
		static const std::vector<std::string_view> definitionOptionsArgs;
		static const std::vector<Options> definitionOptions;
		argparse::ArgumentParser argumentParser;

		std::vector<std::string> inputArgs;
		void init(std::vector<std::string> args);
		void init(int argc, char** argv);

	  public:
		// Provide additional overloads if I want to reuse the Setup to parse args
		Setup(int argc, char** argv);
		Setup(std::vector<std::string> args);

		Options GetOptions();

		[[nodiscard]] std::vector<settings::StringSettings> GetSettings() const;
		void loadSettings(const std::string&);
		void saveSettings(const std::string&);
		[[nodiscard]] settings::SystemConfiguration GetConfiguration() const;
		void loadConfig(const std::string&);
		void saveConfig(const std::string&);
	};
} // namespace engine::setup
