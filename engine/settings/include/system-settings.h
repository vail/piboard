
#pragma once

#include "properties.h"

#include <cstddef>
#include <yaml-cpp/emitter.h>
#include <yaml-cpp/node/node.h>

namespace engine::settings
{
	static constexpr int MAX_NOTE_COUNT = 88;

	class SystemConfiguration
	{
		friend class YAML::convert<SystemConfiguration>;

		template<typename T>
		class ReadOnlyProperty : public Property<T>
		{
			friend class SystemConfiguration;
			friend class YAML::convert<SystemConfiguration>;

		  public:
			ReadOnlyProperty(const T& _value) : Property<T>(_value) {}

		  private:
			T& operator=(const T& f) override { return Property<T>::operator=(f); }
		};

	  public:
		SystemConfiguration();
		SystemConfiguration(const SystemConfiguration&) = default;
		SystemConfiguration(SystemConfiguration&&) = default;
		~SystemConfiguration() = default;

		SystemConfiguration& operator=(const SystemConfiguration&);
		SystemConfiguration& operator=(SystemConfiguration&&) noexcept;

		ReadOnlyProperty<double> TimeIncrement;
		ReadOnlyProperty<int> NoteCount;
		ReadOnlyProperty<int> Harmonics;
		ReadOnlyProperty<int> BaseFrequency;
		ReadOnlyProperty<bool> IsConvientialOrientation;

		static size_t PropertiesToRead() { return num_properties; }

	  private:
		static const size_t num_properties = 5;
	};
} // namespace engine::settings

namespace YAML
{
	template<>
	struct convert<engine::settings::SystemConfiguration>
	{
		static Node encode(const engine::settings::SystemConfiguration& data)
		{
			Node node;

			node["timeIncrement"] = data.TimeIncrement();
			node["noteCount"] = data.NoteCount();
			node["harmonics"] = data.Harmonics();
			node["baseFrequency"] = data.BaseFrequency();
			node["isConviential"] = data.IsConvientialOrientation();

			return node;
		}

		static bool decode(const Node& node, engine::settings::SystemConfiguration& data)
		{
			if(node.size() != engine::settings::SystemConfiguration::PropertiesToRead())
			{
				return false;
			}

			data.TimeIncrement = node["timeIncrement"].as<double>();
			data.NoteCount = node["noteCount"].as<int>();
			data.Harmonics = node["harmonics"].as<int>();
			data.BaseFrequency = node["baseFrequency"].as<int>();
			data.IsConvientialOrientation = node["isConviential"].as<bool>();
			return true;
		}
	};
} // namespace YAML
