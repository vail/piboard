#include <yaml-cpp/emitter.h>
#include <yaml-cpp/yaml.h>

namespace engine::settings
{
	class ISoundString
	{
	  public:
		virtual ~ISoundString() = default;
		virtual void Calculate() = 0;
		virtual double CalculateTension(int) = 0;

		[[nodiscard]] virtual double GetModulus() const = 0;
		[[nodiscard]] virtual double GetRadius() const = 0;
		[[nodiscard]] virtual double GetDensity() const = 0;
		[[nodiscard]] virtual double GetLength() const = 0;
		[[nodiscard]] virtual double GetFrequency() const = 0;

		virtual void SetModulus(double) = 0;
		virtual void SetRadius(double) = 0;
		virtual void SetDensity(double) = 0;
		virtual void SetLength(double) = 0;
		virtual void SetFrequency(double) = 0;
	};
} // namespace engine::settings

namespace YAML
{
	template<>
	struct convert<engine::settings::ISoundString>
	{
		static Node encode(const engine::settings::ISoundString& data)
		{
			Node node;

			node["modulus"] = data.GetModulus();
			node["radius"] = data.GetRadius();
			node["density"] = data.GetDensity();
			node["length"] = data.GetLength();
			node["frequency"] = data.GetFrequency();

			return node;
		}

		static bool decode(const Node& node, engine::settings::ISoundString& data)
		{
			auto readValue = [&](const std::string& type, void (engine::settings::ISoundString::*func)(double))
			{
				if(node[type].IsDefined())
				{
					(data.*func)(node[type].as<double>());
					return true;
				}
				return false;
			};

			bool result = false;
			result |= readValue("modulus", &engine::settings::ISoundString::SetModulus);
			result |= readValue("radius", &engine::settings::ISoundString::SetRadius);
			result |= readValue("density", &engine::settings::ISoundString::SetDensity);
			result |= readValue("length", &engine::settings::ISoundString::SetLength);
			result |= readValue("frequency", &engine::settings::ISoundString::SetFrequency);

			return result;
		}
	};
} // namespace YAML
