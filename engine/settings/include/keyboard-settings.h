
#pragma once

#include "properties.h"
#include "string-interface.h"

#include <cstddef>
#include <vector>
#include <yaml-cpp/emitter.h>
#include <yaml-cpp/yaml.h>

namespace engine::settings
{
	class SystemConfiguration;
}

namespace engine::settings
{
	class StringSettings final : public ISoundString
	{
		friend class YAML::convert<StringSettings>;
		friend class StringSettingsTester;

		class StringProperty : public Property<double>
		{
		  public:
			StringProperty(StringSettings* _owner) : Property(0.0), owner(_owner) {}
			StringProperty(StringSettings* _owner, double inValue) : Property(inValue), owner(_owner) {}
			double& operator=(const double& inputValue) override
			{
				value = inputValue;
				owner->Calculate();
				return value;
			}

		  private:
			StringSettings* owner;
		};

		class ReadOnlyProperty : public Property<double>
		{
			friend class StringSettings;

		  public:
			ReadOnlyProperty(double _value) : Property(_value) {}

		  private:
			double& operator=(const double& f) override { return Property<double>::operator=(f); }
		};

	  public:
		StringSettings();
		StringSettings(const ISoundString&);
		StringSettings(ISoundString&&);
		StringSettings(double, double, double, double);
		StringSettings(const StringSettings&) = default;
		StringSettings(StringSettings&&) = default;
		~StringSettings() final = default;

		StringSettings& operator=(const StringSettings&);
		StringSettings& operator=(StringSettings&&) noexcept;
		bool operator==(const StringSettings& rhs) const;

		static size_t PropertiesToRead() { return num_properties; }
		bool IsValid() { return isValid; }
		double Frequency(int value);
		std::vector<double> GetInharmonics(int value, const SystemConfiguration& system);

		StringProperty Modulus;
		StringProperty Radius;
		StringProperty Density;
		StringProperty Length;
		ReadOnlyProperty BaseFrequency;
		ReadOnlyProperty Inharmonic;
		ReadOnlyProperty Tension;

	  protected:
		[[nodiscard]] double GetModulus() const override { return Modulus(); }
		[[nodiscard]] double GetRadius() const override { return Radius(); }
		[[nodiscard]] double GetDensity() const override { return Density(); }
		[[nodiscard]] double GetLength() const override { return Length(); }
		[[nodiscard]] double GetFrequency() const override { return BaseFrequency(); }

		void SetModulus(double value) override { Modulus = value; }
		void SetRadius(double value) override { Radius = value; }
		void SetDensity(double value) override { Density = value; }
		void SetLength(double value) override { Length = value; }
		void SetFrequency(double value) override
		{ // TODO: Determine Frequency
		}

	  private:
		void Calculate() override;
		double CalculateTension(int value) override;

		static const size_t num_properties = 4;
		bool isValid;
	};
} // namespace engine::settings

namespace YAML
{
	template<>
	struct convert<engine::settings::StringSettings>
	{
		static Node encode(const engine::settings::StringSettings& data)
		{
			Node node;

			node["modulus"] = data.Modulus();
			node["radius"] = data.Radius();
			node["density"] = data.Density();
			node["length"] = data.Length();

			return node;
		}

		static bool decode(const Node& node, engine::settings::StringSettings& data)
		{
			if(node.size() != engine::settings::StringSettings::PropertiesToRead())
			{
				return false;
			}

			data.Modulus = node["modulus"].as<double>();
			data.Radius = node["radius"].as<double>();
			data.Density = node["density"].as<double>();
			data.Length = node["length"].as<double>();
			data.Calculate();
			return true;
		}
	};
} // namespace YAML
