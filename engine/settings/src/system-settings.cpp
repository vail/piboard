
#include "system-settings.h"

#include <utility>

namespace engine::settings
{
	static const double DEFAULT_TIME_INC = 0.05;
	static const int DEFAULT_HAROMINCS = 2;
	static const int DEFAULT_BASE_FREQ = 144;

	SystemConfiguration::SystemConfiguration() :
		TimeIncrement(DEFAULT_TIME_INC), NoteCount(MAX_NOTE_COUNT), Harmonics(DEFAULT_HAROMINCS),
		BaseFrequency(DEFAULT_BASE_FREQ), IsConvientialOrientation(true)
	{
	}

	SystemConfiguration& SystemConfiguration::operator=(const SystemConfiguration& other)
	{
		TimeIncrement = other.TimeIncrement.value;
		NoteCount = other.NoteCount.value;
		Harmonics = other.Harmonics.value;
		BaseFrequency = other.BaseFrequency.value;
		IsConvientialOrientation = other.IsConvientialOrientation.value;

		return *this;
	}

	SystemConfiguration& SystemConfiguration::operator=(SystemConfiguration&& other) noexcept
	{
		TimeIncrement = std::move(other.TimeIncrement.value);
		NoteCount = other.NoteCount.value;
		Harmonics = other.Harmonics.value;
		BaseFrequency = other.BaseFrequency.value;
		IsConvientialOrientation = other.IsConvientialOrientation.value;

		return *this;
	}

} // namespace engine::settings
