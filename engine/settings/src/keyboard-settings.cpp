
#include "keyboard-settings.h"

#include "system-settings.h"

#include <cmath>
#include <limits>
#include <numbers>

constexpr double baseFreq()
{
	const double base_frequency = 440.0;
	const double div_factor = 49.0 / 12.0;
	const double power_factor = std::pow(2, div_factor);
	return base_frequency / power_factor;
}

namespace engine::settings
{
	static const double step = 1 / 12.0;
	static const double factor16 = 16.0;

	StringSettings::StringSettings() :
		Modulus(this), Radius(this), Density(this), Length(this), BaseFrequency(baseFreq()), Inharmonic(0.0),
		Tension(0.0), isValid(false)
	{
	}

	StringSettings::StringSettings(double inModulus, double inRadius, double inDensity, double inLength) :
		Modulus(this, inModulus), Radius(this, inRadius), Density(this, inDensity), Length(this, inLength),
		BaseFrequency(baseFreq()), Inharmonic(0.0), Tension(CalculateTension(0)), isValid(true)
	{
		Calculate();
	}

	StringSettings& StringSettings::operator=(const StringSettings& rhs)
	{
		Modulus = rhs.Modulus();
		Radius = rhs.Radius();
		Density = rhs.Density();
		Length = rhs.Length();
		BaseFrequency = rhs.BaseFrequency();
		Inharmonic = rhs.Inharmonic();
		Tension = rhs.Tension();
		isValid = rhs.isValid;

		return *this;
	}

	StringSettings& StringSettings::operator=(StringSettings&& rhs) noexcept
	{
		Modulus = rhs.Modulus();
		Radius = rhs.Radius();
		Density = rhs.Density();
		Length = rhs.Length();
		BaseFrequency = rhs.BaseFrequency();
		Inharmonic = rhs.Inharmonic();
		Tension = rhs.Tension();
		isValid = rhs.isValid;

		return *this;
	}

	inline static bool Compare_Double(double x, double y)
	{
		return std::fabs(x - y) < std::numeric_limits<double>::epsilon();
	}

	bool StringSettings::operator==(const StringSettings& rhs) const
	{
		bool modCheck = Compare_Double(Modulus(), rhs.Modulus());
		bool radCheck = Compare_Double(Radius(), rhs.Radius());
		bool denCheck = Compare_Double(Density(), rhs.Density());
		bool lenCheck = Compare_Double(Length(), rhs.Length());
		bool baseCheck = Compare_Double(BaseFrequency(), rhs.BaseFrequency());
		bool inharmCheck = Compare_Double(Inharmonic(), rhs.Inharmonic());
		bool tenCheck = Compare_Double(Tension(), rhs.Tension());
		bool validCheck = isValid == rhs.isValid;

		return modCheck && radCheck && denCheck && lenCheck && baseCheck && inharmCheck && tenCheck && validCheck;
	}

	void StringSettings::Calculate()
	{
		BaseFrequency = baseFreq() * std::pow(1, step);

		const double pi2 = std::numbers::pi * std::numbers::pi;
		double radius_squared = Radius() * Radius();
		double length_quart = Length() * Length() * Length() * Length();
		double freq_squared = BaseFrequency() * BaseFrequency();
		Inharmonic = pi2 * Modulus() * radius_squared / factor16 * Density() * length_quart * freq_squared;
	}

	double StringSettings::CalculateTension(int value)
	{
		const double length_squared = Length() * Length();
		int new_freq_num = ((value + 1) << 2);
		double new_freq = std::pow(new_freq_num, step) * baseFreq();
		double freq_squared_new = new_freq * new_freq;
		double new_tension = 4 * Density() * length_squared * freq_squared_new * std::numbers::pi * Radius() * Radius();

		return new_tension;
	}

	double StringSettings::Frequency(int value)
	{
		// const double length_squared = length * length;
		int new_freq_num = ((value + 1) << 2);
		double new_freq = std::pow(new_freq_num, step) * baseFreq();
		double adj_freq = BaseFrequency() * value * sqrt(1 + Inharmonic() * value * value);
		return (new_freq + adj_freq / 2);
	}

	std::vector<double> StringSettings::GetInharmonics(int value, const SystemConfiguration& system)
	{
		auto numInharmonics = static_cast<size_t>(system.Harmonics());
		std::vector<double> inharmonics;
		inharmonics.resize(numInharmonics);

		// TODO: Calculate the inharmonics;

		return inharmonics;
	}
} // namespace engine::settings
