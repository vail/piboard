
#include "waveAmplitude.h"

namespace engine::engine
{
	WaveAmplitude::WaveAmplitude(double frequency) : Frequency(frequency), amplitude {} {}

	void WaveAmplitude::InitStorageContainer(size_t size)
	{
		amplitude.reserve(size);
	}

	const std::vector<double> &WaveAmplitude::GetAmplitudes() const
	{
		return amplitude;
	}

	double WaveAmplitude::GetLastAmplitude() const
	{
		return amplitude.back();
	}

	void WaveAmplitude::PutAmpltiude(double value)
	{
		amplitude.push_back(value);
	}
} // namespace engine::engine
