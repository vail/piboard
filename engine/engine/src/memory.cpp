
#include "memory.h"

#include "system-settings.h"
#include "vector.h"
#include "waveAmplitude.h"

#include <vector>

namespace engine::engine
{
	WaveGroup::WaveGroup(const WaveGroup& other) :
		WavePosition(other.WavePosition()), waveAmplitudes(other.waveAmplitudes), configuration(other.configuration)
	{
	}

	WaveGroup::WaveGroup(const settings::SystemConfiguration& _config) :
		WavePosition({0, 0, 0, 0}), waveAmplitudes(), configuration(&_config)
	{
	}

	bool WaveGroup::operator==(const WaveGroup& rhs)
	{
		return WavePosition.value == rhs.WavePosition.value;
	}

	void WaveGroup::IncrementTime()
	{
		WavePosition.value += configuration->TimeIncrement();
	}

	void WaveGroup::IncrementPosition(double xInc, double yInc, double zInc)
	{
		WavePosition.value += {xInc, yInc, zInc};
	}

	void WaveGroup::IncrementPosition(const Vec3d& inc)
	{
		WavePosition.value += inc;
	}

	bool WaveGroup::CanUpdate(const std::vector<double>& values) const
	{
		return values.size() == waveAmplitudes.size();
	}

	bool WaveGroup::UpdateAmplitudes(const std::vector<double>& values)
	{
		bool canUpdate = CanUpdate(values);
		if(!canUpdate)
		{
			return false;
		}

		auto waveItr = waveAmplitudes.begin();
		auto valueItr = values.begin();
		for(; waveItr != waveAmplitudes.end(); waveItr++, valueItr++)
		{
			waveItr->PutAmpltiude(*valueItr);
		}
		return true;
	}

	const std::vector<WaveAmplitude>& WaveGroup::GetAmplitudes() const
	{
		return waveAmplitudes;
	}

	const WaveAmplitude& WaveGroup::GetPrimaryAmplitude() const
	{
		return waveAmplitudes.front();
	}

} // namespace engine::engine
