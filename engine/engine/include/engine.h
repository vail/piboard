
#pragma once

#include "keyboard-settings.h"
#include "memory.h"
#include "settings-setup.h"
#include "system-settings.h"

#include <vector>

namespace engine::engine
{
	class Calculator
	{
	  public:
		Calculator(setup::Setup);

	  private:
		void GenerateBaseWaves();
		std::vector<WaveGroup> waves;
		settings::SystemConfiguration systemConfig;
		std::vector<settings::StringSettings> stringSettings;
	};
} // namespace engine::engine
