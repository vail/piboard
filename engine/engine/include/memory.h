
#pragma once

#include "position.h"
#include "properties.h"
#include "waveAmplitude.h"

#include <vector>

namespace engine
{
	class Vec3d;
	namespace settings
	{
		class SystemConfiguration;
	}
} // namespace engine

namespace engine::engine
{
	class WaveGroup
	{
		class ReadOnlyProperty : public Property<Position>
		{
			friend class WaveGroup;

		  public:
			ReadOnlyProperty(const Position &pos) : Property<Position>(pos) {}

		  private:
			Position &operator=(const Position &f) override { return value = f; }
		};

	  public:
		WaveGroup(WaveGroup &&) = delete;
		WaveGroup &operator=(const WaveGroup &) = delete;
		WaveGroup &operator=(WaveGroup &&) = delete;
		WaveGroup(const settings::SystemConfiguration &);
		WaveGroup(const WaveGroup &);
		~WaveGroup() = default;

		bool operator==(const WaveGroup &);

		void IncrementTime();
		void IncrementPosition(double, double, double);
		void IncrementPosition(const Vec3d &);

		[[nodiscard]] bool CanUpdate(const std::vector<double> &) const;
		bool UpdateAmplitudes(const std::vector<double> &);
		[[nodiscard]] const std::vector<WaveAmplitude> &GetAmplitudes() const;
		[[nodiscard]] const WaveAmplitude &GetPrimaryAmplitude() const;

		ReadOnlyProperty WavePosition;

	  private:
		std::vector<WaveAmplitude> waveAmplitudes;
		const settings::SystemConfiguration *configuration;
	};
} // namespace engine::engine
