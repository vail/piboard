
#pragma once

#include "properties.h"

#include <cstddef>
#include <vector>

namespace engine::engine
{
	class WaveAmplitude
	{
		template<typename T>
		class ReadOnlyProperty : public Property<T>
		{
			friend class wavePosition;

		  public:
			ReadOnlyProperty(const T &inValue) : Property<T>(inValue) {}

		  private:
			T &operator=(const T &f) override { return Property<T>::operator=(f); }
		};

	  public:
		WaveAmplitude(double);

		void InitStorageContainer(size_t size);

		[[nodiscard]] const std::vector<double> &GetAmplitudes() const;
		[[nodiscard]] double GetLastAmplitude() const;
		void PutAmpltiude(double value);

		ReadOnlyProperty<double> Frequency;

	  private:
		std::vector<double> amplitude;
	};
} // namespace engine::engine
