#include <yaml-cpp/emitter.h>
#include <yaml-cpp/yaml.h>

namespace keyboard::ui::model
{
	struct StringProperty
	{
		double minValue;
		double maxValue;
		double step;
		double largeStep;
		double defaultValue;
	};
} // namespace keyboard::ui::model

namespace YAML
{
	template<>
	struct convert<keyboard::ui::model::StringProperty>
	{
		static Node encode(const keyboard::ui::model::StringProperty& data)
		{
			Node node;

			node["minValue"] = data.minValue;
			node["maxValue"] = data.maxValue;
			node["step"] = data.step;
			node["defaultValue"] = data.defaultValue;

			return node;
		}

		static bool decode(const Node& node, keyboard::ui::model::StringProperty& data)
		{
			if(node.size() != 4)
			{
				return false;
			}

			data.minValue = node["minValue"].as<double>();
			data.maxValue = node["maxValue"].as<double>();
			data.step = node["step"].as<double>();
			data.defaultValue = node["defaultValue"].as<double>();
			return true;
		}
	};
} // namespace YAML
