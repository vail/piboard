
#pragma once

#include <../../engine/settings/include/string-interface.h>

namespace keyboard::ui::model
{
	class StringModel final : engine::settings::ISoundString
	{
	  public:
		StringModel();
		StringModel(double, double, double, double, int);
		StringModel(const engine::settings::ISoundString &);
		StringModel(engine::settings::ISoundString &&);
		StringModel(const StringModel &) = default;
		StringModel(StringModel &&) = default;
		~StringModel() final = default;

		StringModel &operator=(const StringModel &) = delete;
		StringModel &operator=(StringModel &&) = delete;
		bool operator!=(const StringModel &) const;
		bool operator==(const StringModel &) const;
		void UpdateValues();
		void UpdateStringNum(int num);
		void ReplaceValue(double modulus, double density, double radius, double length);

		[[nodiscard]] double GetModulus() const override { return _modulus; }
		[[nodiscard]] double GetRadius() const override { return _radius; }
		[[nodiscard]] double GetDensity() const override { return _density; }
		[[nodiscard]] double GetLinearDensity() const { return _linearDensity; }
		[[nodiscard]] double GetLength() const override { return _length; }
		[[nodiscard]] double GetFrequency() const override { return _frequency; }
		[[nodiscard]] double GetTestFrequency() const { return _testFrequency; }
		[[nodiscard]] double GetTension() const { return _tension; }
		[[nodiscard]] int GetStringNum() const { return _stringNum; }

		void SetModulus(double value) override { _modulus = value; }
		void SetRadius(double value) override { _radius = value; }
		void SetDensity(double value) override { _density = value; }
		void SetLength(double value) override { _length = value; }
		void SetFrequency(double value) override { _frequency = value; }

	  protected:
		void Calculate() override;
		double CalculateTension(int) override;

	  private:
		double _modulus;
		double _radius;
		double _density;
		double _linearDensity;
		double _length;
		int _stringNum;
		double _tension;
		double _frequency;
		double _testFrequency;
	};
} // namespace keyboard::ui::model
