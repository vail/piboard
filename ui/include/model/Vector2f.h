#pragma once

#include <memory>
#include <vector>
namespace keyboard::ui::model
{
	struct Vector2f
	{
		Vector2f() : X(0.0f), Y(0.0) {}
		Vector2f(float val1, float val2) : X(val1), Y(val2) {}
		Vector2f(double val1, double val2) : X(static_cast<float>(val1)), Y(static_cast<float>(val2)) {}

		float X;
		float Y;
	};

	struct PlotableValues
	{
		PlotableValues() : ptrLength(0) {}
		PlotableValues(const PlotableValues& input)
		{
			if(input.plot)
			{
				plot = std::make_unique<Vector2f*>(*input.plot);
			}
		}

		PlotableValues(std::vector<Vector2f> input) :
			plot(std::make_unique<Vector2f*>(new Vector2f[input.size()])), ptrLength(input.size())
		{
			for(size_t i = 0; i < input.size(); i++)
			{
				(*plot.get())[i] = input[i];
			}
		}
		PlotableValues(PlotableValues&& other) noexcept = default;

		PlotableValues& operator=(PlotableValues&& rhs) noexcept = default;
		PlotableValues& operator=(PlotableValues& rhs)
		{
			plot.swap(rhs.plot);
			ptrLength = rhs.ptrLength;
			return *this;
		}

		std::unique_ptr<Vector2f*> plot;
		size_t ptrLength;
	};

} // namespace keyboard::ui::model
