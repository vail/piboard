#pragma once

#include "model/Vector2f.h"

#include <optional>

namespace keyboard::ui::view
{
	class StringPlotView
	{
	  public:
		StringPlotView();

		int GetSelectedGraphType() { return selectedGraphType; }
		void ShowGraphWindow();

		void SetGraphData(model::PlotableValues& values);

	  protected:
		void PlotGraph();

		model::PlotableValues plotValue;
		std::string GetPlotName();
		int selectedGraphType;
		// Class for seeing how off the frequency is for each string
		// Also show the inharmonics
	};
} // namespace keyboard::ui::view
