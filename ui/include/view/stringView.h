#pragma once

namespace keyboard::ui::model
{
	class StringModel;
}

namespace keyboard::ui::view
{
	class StringView
	{
	  public:
		StringView();

		void UpdateCurrentString(model::StringModel* model);
		int GetAppliedSettings() { return applySettingsOption; }
		void CreateUI();

	  private:
		void UpdateValues();

		double modulusValue;
		double densityValue;
		double linearDensityValue;
		double radiusValue;
		double lengthValue;
		double frequencyValue;
		double testFrequencyValue;
		double tensionValue;
		int stringNumber;
		int applySettingsOption;

		model::StringModel* currentModel = nullptr;
	};
} // namespace keyboard::ui::view
