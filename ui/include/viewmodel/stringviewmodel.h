#pragma once

#include "model/Vector2f.h"
#include "model/stringModel.h"

#include <filesystem>
#include <vector>

namespace keyboard::ui::view_model
{
	class StringViewModel
	{
	  public:
		StringViewModel();

		[[nodiscard]] std::vector<model::StringModel> GetStrings() const;
		[[nodiscard]] model::StringModel* GetCurrentString() const;
		[[nodiscard]] model::PlotableValues GetPlotableValue(int valueType) const;
		void UpdateSelectedString(int);

		void UpdateAllStrings();
		void UpdateSequentialStrings();

		void LoadConfiguration(const std::filesystem::path& path);
		void SaveConfiguration(const std::filesystem::path& path);
		void NewConfiguration();

	  private:
		std::vector<model::StringModel> strings;
		model::StringModel* selectedString;
	};
} // namespace keyboard::ui::view_model
