#pragma once

#include "view/stringPlotView.h"
#include "view/stringView.h"
#include "viewmodel/stringviewmodel.h"

namespace keyboard::ui
{
	class MainWindow
	{
	  public:
		MainWindow();

		void DisplayMenuBar();
		std::pair<bool, bool> DisplayMainWindow();
		void StringSettingWindow();
		void StringSoundWindow();
		void StringPlottingWindow();

	  private:
		void GetDefaultValues();

		int selectedStringSetting;
		bool showSettingsWindow;
		bool showStringPlot;
		view::StringPlotView plotView;
		view::StringView view;
		view_model::StringViewModel controller;
	};
} // namespace keyboard::ui
