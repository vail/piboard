#pragma once

namespace keyboard::ui
{
	inline const double MODULUS_MIN = 0;
	inline const double MODULUS_MAX = 200;
	inline const double MODULUS_STEP = 1;
	inline const double MODULUS_DEFAULT_VALUE = 1;
	inline const double DENSITY_MIN = 1000;
	inline const double DENSITY_MAX = 10000;
	inline const double DENSITY_STEP = 100;
	inline const double DENSITY_DEFAULT_VALUE = 7850;
	inline const double RADIUS_MIN = 1e-4;
	inline const double RADIUS_MAX = 1e-1;
	inline const double RADIUS_STEP = 1e-4;
	inline const double RADIUS_DEFAULT_VALUE = 5e-4;
	inline const double LENGTH_MIN = 0.0;
	inline const double LENGTH_MAX = 1.0;
	inline const double LENGTH_STEP = .001;
	inline const double LENGTH_DEFAULT_VALUE = 0.665;
	inline const double TENSION_MIN = 0;
	inline const double TENSION_MAX = 2000;
	inline const double TENSION_STEP = 1;
	inline const double TENSION_DEFAULT_VALUE = 100;

	inline const int NOTE_COUNT = 89;
} // namespace keyboard::ui
