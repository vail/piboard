#pragma once

#include <filesystem>
#include <memory>

namespace keyboard::ui::model
{
	struct StringProperty;
}

namespace keyboard::ui::defaults
{
	class DefaultLoader
	{
	  public:
		DefaultLoader(const DefaultLoader&) = delete;
		DefaultLoader& operator=(const DefaultLoader&) = delete;

		DefaultLoader(DefaultLoader&&) = delete;
		DefaultLoader& operator=(DefaultLoader&&) = delete;
		~DefaultLoader() = default;

		static DefaultLoader& defaults();

		bool LoadDefaults();
		bool LoadDefaults(const std::filesystem::path&);
		void SaveDefaults();

		std::unique_ptr<model::StringProperty> modulus;
		std::unique_ptr<model::StringProperty> radius;
		std::unique_ptr<model::StringProperty> density;
		std::unique_ptr<model::StringProperty> length;
		std::unique_ptr<model::StringProperty> tension;

	  private:
		DefaultLoader();
	};
} // namespace keyboard::ui::defaults
