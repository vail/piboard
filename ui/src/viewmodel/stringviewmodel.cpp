
#include "viewmodel/stringviewmodel.h"

#include "defaults/defaults.h"
#include "model/Vector2f.h"
#include "model/stringModel.h"

#include <fstream>
#include <optional>
#include <vector>
#include <yaml-cpp/node/parse.h>

namespace keyboard::ui::view_model
{

	StringViewModel::StringViewModel() : strings(NOTE_COUNT, model::StringModel()), selectedString(&*strings.begin())
	{
		int num = 0;
		for(auto& string : strings)
		{
			string.UpdateStringNum(num);
			num++;
		}
	}

	model::StringModel* StringViewModel::GetCurrentString() const
	{
		return selectedString;
	}

	std::vector<model::StringModel> StringViewModel::GetStrings() const
	{
		return strings;
	}

	model::PlotableValues StringViewModel::GetPlotableValue(int valueType) const
	{
		switch(valueType)
		{
			case 1:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetTestFrequency() - string.GetFrequency();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 2:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetModulus();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 3:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetDensity();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 4:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetRadius();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 5:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetLength();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 6:
			{
				std::vector<model::Vector2f> positions;
				for(const auto& string : strings)
				{
					double diff = string.GetTension();
					positions.emplace_back(diff, string.GetStringNum());
				}
				model::PlotableValues values(positions);
				return values;
			}
			case 0:
			default:
			{
				return model::PlotableValues();
			}
		}
	}

	void StringViewModel::UpdateSelectedString(int index)
	{
		if(index > NOTE_COUNT || index < 0)
		{
			return;
		}

		selectedString = &strings[index];
	}

	void StringViewModel::UpdateAllStrings()
	{
		for(auto& string : strings)
		{
			string.ReplaceValue(selectedString->GetModulus(),
								selectedString->GetDensity(),
								selectedString->GetRadius(),
								selectedString->GetLength());
		}
	}

	void StringViewModel::UpdateSequentialStrings()
	{
		auto pos = static_cast<size_t>(selectedString->GetStringNum());
		for(size_t i = pos; i < strings.size(); i++)
		{
			strings[i].ReplaceValue(selectedString->GetModulus(),
									selectedString->GetDensity(),
									selectedString->GetRadius(),
									selectedString->GetLength());
		}
	}

	void StringViewModel::LoadConfiguration(const std::filesystem::path& path)
	{
		if(path.empty() || !std::filesystem::exists(path))
		{
			// TODO: Notify user nothing happens
		}

		strings.clear();
		YAML::Node nodeList = YAML::LoadFile(path);
		strings.reserve(nodeList.size());
		for(const auto& node : nodeList)
		{
			strings.push_back(node.as<model::StringModel>());
		}
	}

	void StringViewModel::SaveConfiguration(const std::filesystem::path& path)
	{
		std::ofstream fout(path);
		YAML::Node nodeList;

		for(const auto& string : strings)
		{
			nodeList.push_back(string);
		}

		fout << nodeList;
		fout.close();
	}

	void StringViewModel::NewConfiguration()
	{
		strings.clear();
		strings.resize(NOTE_COUNT);
		int num = 0;
		for(auto& string : strings)
		{
			string.UpdateStringNum(num);
			num++;
		}
	}

} // namespace keyboard::ui::view_model
