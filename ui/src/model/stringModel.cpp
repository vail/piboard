
#include "model/stringModel.h"

#include "defaults/defaultLoader.h"
#include "model/StringProperty.h"

#include <cmath>
#include <numbers>

constexpr double baseFreq()
{
	const double base_frequency = 440.0;
	const double div_factor = 49.0 / 12.0;
	const double power_factor = std::pow(2, div_factor);
	return base_frequency / power_factor;
}

namespace keyboard::ui::model
{
	static const double step = 1 / 12.0;

	StringModel::StringModel() :
		_modulus(defaults::DefaultLoader::defaults().modulus->defaultValue),
		_radius(defaults::DefaultLoader::defaults().radius->defaultValue),
		_density(defaults::DefaultLoader::defaults().density->defaultValue),
		_length(defaults::DefaultLoader::defaults().length->defaultValue), _stringNum(0),
		_tension(CalculateTension(_stringNum)), _frequency(0), _testFrequency(0)
	{
		Calculate();
	}

	StringModel::StringModel(double modulus, double radius, double density, double length, int stringPosition) :
		_modulus(modulus), _radius(radius), _density(density), _length(length), _stringNum(stringPosition),
		_tension(CalculateTension(_stringNum)), _frequency(0), _testFrequency(0)
	{
		Calculate();
	}

	bool StringModel::operator!=(const StringModel& rhs) const
	{
		return !operator==(rhs);
	}

	bool StringModel::operator==(const StringModel& rhs) const
	{
		bool result = this->_stringNum == rhs._stringNum;
		return result;
	}

	void StringModel::ReplaceValue(double modulus, double density, double radius, double length)
	{
		_modulus = modulus;
		_radius = radius;
		_density = density;
		_length = length;
		UpdateValues();
	}

	void StringModel::UpdateStringNum(int num)
	{
		_stringNum = num;
		UpdateValues();
	}

	void StringModel::UpdateValues()
	{
		_linearDensity = std::numbers::pi * _radius * _radius * _density;
		_tension = CalculateTension(_stringNum);
		Calculate();
	}

	void StringModel::Calculate()
	{
		__uint128_t new_freq_num = (__uint128_t)1 << _stringNum;
		_testFrequency = std::pow(new_freq_num, step) * baseFreq();
		_frequency = sqrt(_tension / _linearDensity) / (2 * _length);
	}

	double StringModel::CalculateTension(int)
	{
		const double length_squared = _length * _length;
		__uint128_t new_freq_num = (__uint128_t)1 << _stringNum;
		double new_freq = std::pow(new_freq_num, step) * baseFreq();
		double freq_squared_new = new_freq * new_freq;
		double new_tension = 4 * length_squared * freq_squared_new * _linearDensity;

		return new_tension;
	}
} // namespace keyboard::ui::model
