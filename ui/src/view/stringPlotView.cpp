#include "view/stringPlotView.h"

#include <cmath>
#include <imgui.h>
#include <implot.h>

namespace keyboard::ui::view
{
	StringPlotView::StringPlotView() : plotValue(), selectedGraphType(0) {}

	void StringPlotView::SetGraphData(model::PlotableValues& values)
	{
		plotValue = values;
	}

	void StringPlotView::ShowGraphWindow()
	{
		ImGui::Begin("Graphing Window");
		ImGui::Text("Graph Type:");
		ImGui::SameLine();
		ImGui::RadioButton("None", &selectedGraphType, 0);
		ImGui::SameLine();
		ImGui::RadioButton("Frequency Difference", &selectedGraphType, 1);
		ImGui::SameLine();
		ImGui::RadioButton("Modulus", &selectedGraphType, 2);
		ImGui::SameLine();
		ImGui::RadioButton("Density", &selectedGraphType, 3);
		ImGui::SameLine();
		ImGui::RadioButton("Radius", &selectedGraphType, 4);
		ImGui::SameLine();
		ImGui::RadioButton("Length", &selectedGraphType, 5);
		ImGui::SameLine();
		ImGui::RadioButton("Tension", &selectedGraphType, 6);
		if(plotValue.ptrLength != 0)
		{
			PlotGraph();
		}

		ImGui::End();
	}

	void StringPlotView::PlotGraph()
	{
		std::string plotName = GetPlotName();

		ImPlot::MapInputDefault();
		ImPlot::StyleColorsAuto();
		ImGui::Separator();

		if(ImPlot::BeginPlot("Simple Plot"))
		{
			model::Vector2f* valueArr = *(plotValue.plot);

			ImPlot::PlotLine(
				plotName.c_str(), &(*valueArr).Y, &(*valueArr).X, plotValue.ptrLength, 0, 0, sizeof(model::Vector2f));
			ImPlot::EndPlot();
		}
	}

	std::string StringPlotView::GetPlotName()
	{
		return "Test";
	}
} // namespace keyboard::ui::view
