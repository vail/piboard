#include "view/stringView.h"

#include "defaults/defaultLoader.h"
#include "model/StringProperty.h"
#include "model/stringModel.h"

#include <imgui.h>
#include <string>

#define defaultShort keyboard::ui::defaults::DefaultLoader::defaults()

namespace keyboard::ui::view
{
	StringView::StringView() :
		modulusValue(defaultShort.modulus->defaultValue), densityValue(defaultShort.density->defaultValue),
		radiusValue(defaultShort.radius->defaultValue), lengthValue(defaultShort.length->defaultValue),
		frequencyValue(0.0), testFrequencyValue(0.0), tensionValue(defaultShort.tension->defaultValue), stringNumber(0),
		applySettingsOption(0)
	{
	}

	void StringView::CreateUI()
	{
		auto DoubleInputs =
			[&](std::string& label, double testValue, double& value, const model::StringProperty* property)
		{
			if(ImGui::InputDouble(label.c_str(),
								  &testValue,
								  property->step,
								  property->largeStep,
								  "%.5f",
								  ImGuiInputTextFlags_::ImGuiInputTextFlags_CharsDecimal))
			{
				if(testValue > property->minValue && testValue < property->maxValue)
				{
					value = testValue;
					UpdateValues();
				}
			}
		};

		std::string title = "String Values " + std::to_string(stringNumber);
		ImGui::Begin(title.c_str());

		std::string modulusString = "Modulus Value";
		std::string densityString = "Density Value";
		std::string radiusString = "Radius Value";
		std::string lengthString = "Length Value";
		std::string tensionString = "Tension Value";
		std::string linearDensityString = "Linear Density Value";

		DoubleInputs(modulusString, modulusValue, modulusValue, defaultShort.modulus.get());
		DoubleInputs(densityString, densityValue, densityValue, defaultShort.density.get());
		DoubleInputs(radiusString, radiusValue, radiusValue, defaultShort.radius.get());
		DoubleInputs(lengthString, lengthValue, lengthValue, defaultShort.length.get());

		ImGui::LabelText("Linear Density", "%s", std::to_string(linearDensityValue).c_str());
		ImGui::LabelText("Frequency", "%s", std::to_string(frequencyValue).c_str());
		ImGui::LabelText("Test Frequency", "%s", std::to_string(testFrequencyValue).c_str());

		// TODO: Need a better way of handling the tension. May need to make small adjustments but
		ImGui::LabelText("Tension", "%s", std::to_string(tensionValue).c_str());
		// DoubleInputs(tensionString, tensionValue, tensionValue, defaultShort.tension.get());

		ImGui::Text("Apply to:");
		ImGui::SameLine();
		ImGui::RadioButton("Current", &applySettingsOption, 0);
		ImGui::SameLine();
		ImGui::RadioButton("All", &applySettingsOption, 1);
		ImGui::SameLine();
		ImGui::RadioButton("Subsequential", &applySettingsOption, 2);

		ImGui::End();
	}

	void StringView::UpdateValues()
	{
		if(currentModel != nullptr)
		{
			currentModel->ReplaceValue(modulusValue, densityValue, radiusValue, lengthValue);
			frequencyValue = currentModel->GetFrequency();
			linearDensityValue = currentModel->GetLinearDensity();
			testFrequencyValue = currentModel->GetTestFrequency();
			tensionValue = currentModel->GetTension();
		}
	}

	void StringView::UpdateCurrentString(model::StringModel* model)
	{
		if(model != nullptr && model != currentModel)
		{
			currentModel = model;
			modulusValue = currentModel->GetModulus();
			radiusValue = currentModel->GetRadius();
			densityValue = currentModel->GetDensity();
			lengthValue = currentModel->GetLength();
			linearDensityValue = currentModel->GetLinearDensity();
			UpdateValues();
		}
	}

} // namespace keyboard::ui::view

#undef defaultShort
