
#include "defaults/defaultLoader.h"

#include "defaults/defaults.h"
#include "model/StringProperty.h"
#include "model/stringModel.h"

#include <filesystem>
#include <fstream>
#include <memory>
#include <vector>
#include <yaml-cpp/node/emit.h>
#include <yaml-cpp/node/parse.h>

namespace fs = std::filesystem;

namespace keyboard::ui::defaults
{
	DefaultLoader::DefaultLoader() :
		modulus(std::make_unique<model::StringProperty>()), radius(std::make_unique<model::StringProperty>()),
		density(std::make_unique<model::StringProperty>()), length(std::make_unique<model::StringProperty>()),
		tension(std::make_unique<model::StringProperty>())
	{
		modulus->minValue = MODULUS_MIN;
		modulus->maxValue = MODULUS_MAX;
		modulus->step = MODULUS_STEP;
		modulus->defaultValue = MODULUS_DEFAULT_VALUE;

		radius->minValue = RADIUS_MIN;
		radius->maxValue = RADIUS_MAX;
		radius->step = RADIUS_STEP;
		radius->defaultValue = RADIUS_DEFAULT_VALUE;

		density->minValue = DENSITY_MIN;
		density->maxValue = DENSITY_MAX;
		density->step = DENSITY_STEP;
		density->defaultValue = DENSITY_DEFAULT_VALUE;

		length->minValue = LENGTH_MIN;
		length->maxValue = LENGTH_MAX;
		length->step = LENGTH_STEP;
		length->defaultValue = LENGTH_DEFAULT_VALUE;

		tension->minValue = TENSION_MIN;
		tension->maxValue = TENSION_MAX;
		tension->step = TENSION_STEP;
		tension->defaultValue = TENSION_DEFAULT_VALUE;
	}

	DefaultLoader& DefaultLoader::defaults()
	{
		static DefaultLoader instance;
		return instance;
	}

	bool DefaultLoader::LoadDefaults()
	{
		fs::path saveFile = fs::current_path() / "defaultFile.yaml";
		return LoadDefaults(saveFile);
	}

	bool DefaultLoader::LoadDefaults(const fs::path& filePath)
	{
		if(filePath.empty() || !fs::exists(filePath))
		{
			return false;
		}

		try
		{
			auto tot = YAML::LoadAllFromFile(filePath);
			modulus = std::make_unique<model::StringProperty>(tot[0]["modulus"].as<model::StringProperty>());
			radius = std::make_unique<model::StringProperty>(tot[0]["radius"].as<model::StringProperty>());
			density = std::make_unique<model::StringProperty>(tot[0]["density"].as<model::StringProperty>());
			length = std::make_unique<model::StringProperty>(tot[0]["length"].as<model::StringProperty>());
			tension = std::make_unique<model::StringProperty>(tot[0]["tension"].as<model::StringProperty>());
		}
		catch(std::exception& e)
		{
			//Something is wrong with the file we're trying to load.
			//Assume the file isn't correct.
			fs::remove(filePath);
			return false;
		}

		return true;
	}

	void DefaultLoader::SaveDefaults()
	{
		fs::path saveFile = fs::current_path() / "defaultFile.yaml";
		std::ofstream fout(saveFile);

		YAML::Node local;
		local["modulus"] = *modulus;
		local["radius"] = *radius;
		local["density"] = *density;
		local["length"] = *length;
		local["tension"] = *tension;
		fout << local;
	}
} // namespace keyboard::ui::defaults
