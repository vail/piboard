
#include "mainWindow.h"

#include "defaults/defaultLoader.h"
#include "defaults/defaults.h"

#include <imgui.h>
#include <string>

namespace keyboard::ui
{
	MainWindow::MainWindow() : selectedStringSetting(0), showSettingsWindow(false), showStringPlot(false), controller()
	{
		GetDefaultValues();
	}

	void MainWindow::DisplayMenuBar()
	{
		ImGui::BeginMenuBar();
		ImGui::BeginMenu("File");
		if(ImGui::MenuItem("New", "Ctrl+N"))
		{
			controller.NewConfiguration();
		}
		if(ImGui::MenuItem("Load", "Ctrl+L"))
		{
			// TODO: find a way to get a file dialog
			controller.LoadConfiguration("");
		}
		if(ImGui::MenuItem("Save", "Ctrl+S"))
		{
			// TODO:: find a way to get a file dialog
			controller.SaveConfiguration("");
		}
	}

	void MainWindow::GetDefaultValues()
	{
		bool loadedDefaults = defaults::DefaultLoader::defaults().LoadDefaults();
		if(!loadedDefaults)
		{
			defaults::DefaultLoader::defaults().SaveDefaults();
		}
		view = view::StringView();
	}

	std::pair<bool, bool> MainWindow::DisplayMainWindow()
	{
		ImGuiIO &io = ImGui::GetIO();
		ImGui::Begin("Key String Selector", nullptr, ImGuiWindowFlags_HorizontalScrollbar);

		// Need to add buttons to select the keys
		// determine which view to select
		static float f = 0.0f;
		static int counter = 0;
		const int MAX_NUMBER_EACH_LINE = 19;

		ImGui::Text("This is some useful text."); // Display some text (you can use a format strings too)

		// Use a horizontal scroll bar
		// Set up radio buttons or normal buttons to handle selecting the key

		for(int i = 0; i < NOTE_COUNT; i++)
		{
			ImGui::RadioButton(std::to_string(i).c_str(), &selectedStringSetting, i);
			if(i != NOTE_COUNT - 1 && i % MAX_NUMBER_EACH_LINE != MAX_NUMBER_EACH_LINE - 1)
			{
				ImGui::SameLine();
			}
		}

		ImGui::Checkbox("Advance Settings Window", &showSettingsWindow);
		ImGui::Checkbox("String Setting Window", &showSettingsWindow);
		ImGui::Checkbox("Show Plot Window", &showStringPlot);

		ImGui::SliderFloat("float", &f, 0.0f, 1.0f); // Edit 1 float using a slider from 0.0f to 1.0f
		// ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

		if(ImGui::Button("Button")) // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;
		ImGui::SameLine();
		ImGui::Text("counter = %d", counter);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
		ImGui::End();

		return std::make_pair(showSettingsWindow, showStringPlot);
	}

	void MainWindow::StringSettingWindow()
	{
		controller.UpdateSelectedString(selectedStringSetting);
		view.UpdateCurrentString(controller.GetCurrentString());
		view.CreateUI();
		int selectedOption = view.GetAppliedSettings();
		if(selectedOption == 1)
		{
			controller.UpdateAllStrings();
		}
		else if(selectedOption == 2)
		{
			controller.UpdateSequentialStrings();
		}
	}

	void MainWindow::StringPlottingWindow()
	{
		auto newValues = controller.GetPlotableValue(plotView.GetSelectedGraphType());
		plotView.SetGraphData(newValues);
		plotView.ShowGraphWindow();
	}

} // namespace keyboard::ui
